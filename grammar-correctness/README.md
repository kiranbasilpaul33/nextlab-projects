Objective 

To classify the sentences in given dataset as gramatically correct or incorrect.

The Process

1.Use the Bert model from transformers package and do pretraining using the openly available source code.
2.Apply the sentence in the given dataset as input to the model and get the result.
3.Obtain output file by appending the output with input file as a column.

The Result

Output file is generated.

To run the project download ipynb notebook and dataset and open in google colab or jupyter notebook.
