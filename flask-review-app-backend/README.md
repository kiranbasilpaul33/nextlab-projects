**Objective**

Objective of this component is to deploy the saved model with a flask backend as a RESTful API. A post request can be  send with the csv file as the parameter and the reviews that have a discrepancy between stars and sentiment are returned. The web application was deployed using Heroku platform

**The Process**

1. Python function for generating the prediction result is defined in the app.py file. It loads the model, predicts the result and sends back the response.Environment variable FLASK_APP is defined in .flaskenv.txt
2. Deployment in Heroku needs a requirements.txt file and  a Procfile both are generated.
3. The entire project is pushed into Github.
4. A placeholder for project deployment is created in Heroku. 
5. The Github repo is connected with Heroku.
6. Project is deployed from Heroku.

How to download and run the project:
- 1.Download the entire project from GitLab
- 2.In local create an environment for the project with required dependencies and frameworks
- 3.Use windows prompt or anaconda prompt for running the app using flask run after opening the prompt inside he project folder.
- 4.The backend is serving requests at  http://127.0.0.1:5000/predict
- 5.Send request as form data from Postman- key: file value: filename.csv



