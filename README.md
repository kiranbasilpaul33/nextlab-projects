# Nextlabs Projects

This project repo is intended to contain answers to all questions in the evaluation for job role of Jr Data Scientist at Nextlabs.

Part 1

1.Regex

(?<=%*{"id":).*?(?=})|(?<=%*{"code":).*?(?=,)


2.Web App for text classification and processing of reviews for Chrome Browser  
   This question has 3 components:

    1.The Python code that fine creates fine tunes and saves the model.
    2.The Python flask backend that serves a RESTful POST request from the frontend. 
    3.The React frontend that interacts with the user, obtains the csv file and sends it to backend in a POST request

    These 3 components are organized as 3 seperate directories within the main project folder as follows:

    Python code for model creation
    :https://gitlab.com/kiranbasilpaul33/nextlab-projects/-/tree/main/flask-review-app-model-creation

    Flask Backend:https://gitlab.com/kiranbasilpaul33/nextlab-projects/-/tree/main/flask-review-app-backend

    React Frontend:https://gitlab.com/kiranbasilpaul33/nextlab-projects/-/tree/main/flask-review-app-frontend

    Each of these components are provided with seperate Readme file to understand their purpose, usage and deployment details.

       

    Live link to the deployed app: https://review-classifier.netlify.app/

3.Exploratory Data Analysis on Browser Ranking Dataset(Correlation of App ID, long description etc with rank)

    EDA is performed with the help of google colab and the resulting ipynb notebook is uploaded in the directory:
    https://gitlab.com/kiranbasilpaul33/nextlab-projects/-/tree/main/browser-ranking









Part 2

1.Grammatical correctness of a sentence using pretrained bert model
    
    Grammatical correctness of an english sentence can be determined using a pretrained NLP model such as BERT. A large part   of the colab notebook for this question is just obtaining the pretrained model by training using the standard dataset and openly available code. This has been highlighted in the notebook.The output dataset has an extra column showing the whether the dataset is gramatically correct or incorrect.

    Directory:https://gitlab.com/kiranbasilpaul33/nextlab-projects/-/tree/main/grammar-correctness

    Issue in accessing the output dataset: Sometimes the webpage may crash when trying to access the output dataset.
    Alternate link for the same is:https://drive.google.com/file/d/1LkvsVSXgcYo_pR-4lIxPJ9wYQWnwIdGC/view?usp=sharing



   
      
