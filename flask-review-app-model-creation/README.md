**Objective**

 The objective of this component is to create a machine learning model for classifying reviews as positive or negative. Support Vector Machine is a supervised Machine Learning algorithm that can be used for classification problems. This can be combined with a TF-IDF(Term Frequency-Inverse Document Frequency) vectorizer for sentiment analysis. The algorithm focuses on obtaining keywords and not on the actual meaning of sentences. In this component the model need to be created, trained, fine tuned, tested and saved for later use.

**The Dataset**

 To train the model IMDB dataset is used. It consist of about 50000 rows which has 2 features- the review and its sentiment (Positive or Negative).Since training on entire dataset is not feasible and may lead to overfitting a rough optimal samples from the dataset consisting of 10000 rows is taken for the usecase. 

**The Process**


- 1.Data Preprocessing: Nan values as well as empty strings are removed. The reviews are then converted into lowercase and whitespaces are stripped.
- 2.Train-test split is generated.
- 3.SVM classifier and TFIDF vectorizer are initialized with default values.
- 4.GridSearchCV is used for cross validation by using a pipeline to apply the SVM and the tfidf vectorizer along with different   combination of hyperparameters passed by the paramgrid.
- 5.Model is trained on training data and validated with test data.
- 6.Model is then retrained with the entire dataset and best values for hyperparameters.
- 7.Model is saved for future use.


**The Result**


- Accuracy of the model:88%
- f1 score : .87
- Hyperparameters of final model: gamma='scale', C=0.7, kernel='linear'


