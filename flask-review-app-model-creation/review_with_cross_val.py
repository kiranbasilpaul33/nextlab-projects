"""
Code for fine tuning SVM model for classifying reviews

"""
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score,f1_score
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
df=pd.read_csv(r'G:\IMDB Dataset.csv') #IMDB dataset is a dataset consisting of reviews for movies.It was chosen for training due to its similarity with given dataset
df=df.sample(10000)  #Taking a small sample of data
df=df.dropna()

df=df[df['review'].str.strip().str.len()!=0]  #Removing entries having empty space only
df['review']=df['review'].str.strip().str.lower() #Removing whitespace and converting to lowecase
X=df['review']
y=df['sentiment']
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.2,random_state=101)
svm_classifier=SVC()
tfidf=TfidfVectorizer()
operations=[('tfidf',tfidf),('svm_classifier',svm_classifier)]
pipe=Pipeline(operations)  #creating pipeling with tdidf vectorizer and svm classifier
kernels=['linear', 'poly', 'rbf', 'sigmoid']
param_grid={'tfidf__stop_words':['english'],'svm_classifier__kernel':kernels,
            'svm_classifier__C':[0.01,0.1,0.5,0.7,1],'svm_classifier__gamma':['scale','auto']}
final_model=GridSearchCV(pipe,param_grid,scoring='accuracy',cv=5)
final_model.fit(X_train,y_train)
print(final_model.best_estimator_.get_params())
y_pred=final_model.predict(X_test)
print(accuracy_score(y_test,y_pred))
print(f1_score(y_test,y_pred))




'''

RESULTS
=======
Accuarcy value:88%
f1 score : .87
Hyperparameters of final model: gamma='scale', C=0.7, kernel='linear'

'''
