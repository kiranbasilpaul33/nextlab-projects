#Code for training the tuned model on entire dataset and saving it
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC
import joblib
df=pd.read_csv(r'G:\IMDB Dataset.csv')
df=df.sample(10000)
df=df.dropna()
df=df[df['review'].str.strip().str.len()!=0]
df['review']=df['review'].str.strip().str.lower()
X=df['review']
y=df['sentiment']
svm_classifier=SVC(C=0.7,gamma='scale',kernel='linear')
tfidf=TfidfVectorizer(stop_words='english')
X=tfidf.fit_transform(X)
svm_classifier.fit(X,y)
joblib.dump(svm_classifier,'svm_model.pkl')
joblib.dump(tfidf,'vectorizer.pkl')

