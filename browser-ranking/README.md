**Objective**

To check correlation between browser rankings and their descriptions, App ID and Rank of a browser.

**The Process**

Perform Exploratory data analysis to understand relationship between various columns.

**Results**

- 1.Length of long description does not affect the rankings.
- 2.Presence of keywords in the first 10 words of a long description can increase the rank of a browser.
- 3.Presence of keywords in short description can increase the rank of a browser.
- 4.Puffin TV, Duck Duck Go and Brave are the best rated browsers while Vivaldi is the words.
- 5.Keywords that can fetch a higher ranking are:browsing features, browsing data, bookmarks, 
- web browser, 
- one tap,
- personal data,
- ad blocker,
- private internet browser,
- privately browse,
- free built

To run the project download ipynb notebook and dataset and open in google colab or jupyter notebook.
